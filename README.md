#### 1. Dependencia:
* Python 2.7.12 

#### 2. Preparando o ambiente virtual:
* Instalando o virtualenv:
```
pip install virtualenv
```
* Criando o ambiente virtual:
```
virtualenv ENV
```
* Ativando o ambiente:
```
source ENV/bin/activate
```

* Desativando o ambiente virtual:
```
deactivate
```
#### 3. Intalando as dependencias:
```
pip install -r requirements.txt
```
#### 4. Instalando Firefox headless no ubuntu:
```
apt-get install firefox
```
#### 5. Baixando e Instalando Webdriver:

* Linux 64Bits
```
wget https://github.com/mozilla/geckodriver/releases/download/v0.21.0/geckodriver-v0.21.0-linux64.tar.gz
```

* Linux 32Bits
```
wget https://github.com/mozilla/geckodriver/releases/download/v0.21.0/geckodriver-v0.21.0-linux32.tar.gz
```

* Decompactando:
```
tar -xvzf geckodriver-v0.21.0-linux64.tar.gz
```

* Copiando para o ambiente virtual:
```
cp geckodriver ENV/bin/
```