import scrapy
import csv
import datetime
from twisted.internet import reactor,defer
from scrapy.utils.log import configure_logging
from scrapy.crawler import CrawlerRunner
from scrapy.http import FormRequest
from scrapy.http import Request
from scrapy import Item, Field
from scrapy.loader import ItemLoader


# Itens of spider class
class Consumption(scrapy.Item):

    user_id = scrapy.Field()
    unidade_consumidora = scrapy.Field()
    data_referencia = scrapy.Field()
    consumo_kwh  = scrapy.Field()


# Celesc Spider crawler class
class Celesc(scrapy.Spider):
    name = "celesc"
    
    password_url = "http://agenciaweb.celesc.com.br:8080/AgenciaWeb/autenticar/autenticar.do"
    
    hist_payment = "http://agenciaweb.celesc.com.br:8080/AgenciaWeb/consultarHistoricoPagto/consultarHistoricoPagto.do"

    hist_consumption = "http://agenciaweb.celesc.com.br:8080/AgenciaWeb/consultarHistoricoConsumo/histCons.do"
    
    allowed_domains = [
                        "http://agenciaweb.celesc.com.br:8080/AgenciaWeb/autenticar/loginCliente.do",
                        "http://agenciaweb.celesc.com.br:8080/AgenciaWeb/autenticar/autenticar.do",
                        "http://agenciaweb.celesc.com.br:8080/AgenciaWeb/consultarHistoricoConsumo/histCons.do"
    ]
    
    start_urls = (
        'http://agenciaweb.celesc.com.br:8080/AgenciaWeb/autenticar/loginCliente.do',
    )


    # Redefine to receive dictionary of user data
    def __init__(self, *args, **kwargs):
        super(Celesc, self).__init__(*args, **kwargs)
 

         # Dictionary of user authentication fileds
        self.AUTH_FIELDS = dict()
        
        # Dictionary of user password field
        self.PASSWD_FIELD = dict()

        # Field search of energy consumption
        self.HIST_RANGE = dict()

        # User Id
        self.id = kwargs.get('id')

        # Indicates that a new user, so we have to get energy consumption range during a period
        self.new_user = False       


        # Unpacking user data from login
        self.AUTH_FIELDS['sqUnidadeConsumidora'] = kwargs.get('sqUnidadeConsumidora')
        self.AUTH_FIELDS['tpDocumento'] = kwargs.get('tpDocumento')
        self.AUTH_FIELDS['numeroDocumentoCPF'] = kwargs.get('numeroDocumentoCPF')
        #self.AUTH_FIELDS['id'] = kwargs.get('id')
        #self.id= kwargs.get('id')


        # Unpacking password user data 
        self.PASSWD_FIELD['senha'] = kwargs.get('senha')
        
        
        # Check historic 
        if 'yes' in kwargs.get('new_user'):
                
            # update state of new user
            self.new_user = True

        else:  

            # update state of new user
            self.new_user = False
        pass



    def parse(self, response):
        #logging.log(logging.INFO,"Efetuando login unidade consumidora e CPF...")
        self.logger.info("Efetuando login unidade consumidora e CPF...")

        return [FormRequest.from_response(
            response,
            formdata = self.AUTH_FIELDS,
            callback = self.password_login, dont_filter=True
        )]


    def password_login(self, response):
        # check login succeed before going on
        #logging.log(logging.INFO,"Efeturando login com senha....")
        self.logger.info("Efeturando login com senha....")

        return [FormRequest.from_response(
            response,
            formdata = self.PASSWD_FIELD,
            callback=self.after_login, dont_filter=True
        )]


    def after_login(self, response):

        #logging.log(logging.INFO,"Verificando login....")
        self.logger.info("Verificando login....")
        
        # Checks login error
        if "avisoErro" in response.body:
            #logging.log(logging.ERROR, "Login failed")
            self.logger.error("Login falhou na unidade consumidora:%s", self.AUTH_FIELDS['sqUnidadeConsumidora'])

            return
        else:

            #logging.log(logging.INFO,"Login efetuado com sucesso...")
            self.logger.info("Login efetuado com sucesso...")
            
            if self.new_user :

                return Request(url=self.hist_consumption,
                    callback=self.consumption_range, dont_filter=True)
            else :

                return Request(url=self.hist_payment,
                    callback=self.month_reference, dont_filter=True)  

    def month_reference(self, response):
        
        #logging.log(logging.INFO,"Pegando o mes de referencia..")
        self.logger.info("Pegando o mes de referencia..")

        # Get refence month and year of energy consumption
        date_reference = ''.join(response.xpath('//*[@id="histFat"]/tbody/tr[1]/td[2]/a/text()').re('(\A\d{2}/\d{4})$'))

        # Check reference moth of payment
        if date_reference :
            
            # Get month reference
            month = date_reference[0:2]
            
            # Get year reference
            year = date_reference[3:8]
            
            # Fill search form of historic energy consumption
            self.HIST_RANGE['mesInicial'] = month
            self.HIST_RANGE['anoInicial'] = year
            self.HIST_RANGE['mesFinal'] = month
            self.HIST_RANGE['anoFinal'] = year

            
            #self.logger.info("Mes de referencia:%s",self.HIST_RANGE)

            return Request(url=self.hist_consumption,
                callback=self.consumption_range, dont_filter=True)

        else :
            #logging.log(logging.ERROR,"Nao foi possivel obter mes de referencia")
            self.logger.error("Nao foi possivel obter mes de referencia na unidade consumidora:%s",self.AUTH_FIELDS['sqUnidadeConsumidora'])
            pass        


    def consumption_range(self, response):
        
        #logging.log(logging.INFO,"Pesquisando o historico de consumo..")
        self.logger.info("Pesquisando o historico de consumo..")
        
        # Checks new user to get a range date of energy consumption
        if self.new_user :

            #Get a range date of energy consumption
            initial_month = ''.join(response.xpath('//*[@id="mesInicial"]/@value').re('\d{2}'))
            initial_year = ''.join(response.xpath('//*[@id="anoInicial"]/@value').re('\d{4}'))
            end_month = ''.join(response.xpath('//*[@id="mesFinal"]/@value').re('\d{2}'))
            end_year = ''.join(response.xpath('//*[@id="anoFinal"]/@value').re('\d{4}'))

            
        
            # Fill search form of historic energy consumption
            self.HIST_RANGE['mesInicial'] = initial_month
            self.HIST_RANGE['anoInicial'] = initial_year
            self.HIST_RANGE['mesFinal'] = end_month
            self.HIST_RANGE['anoFinal'] = end_year


        #self.logger.info("Mes inicial:%s", self.HIST_RANGE)



        return [FormRequest.from_response(
            response,
            formdata = self.HIST_RANGE,
            callback=self.get_consumption, dont_filter=True
        )]
    

    def get_consumption(self, response):
        
        self.logger.info("Extraindo dados do consumo..")


        # Loader data into Consumption Items
        loader = ItemLoader(item=Consumption(), response=response)

        # Reactive and inductive energy consumption
        reat = False

        # Gets user id
        loader.add_value('user_id',str(self.id) )
        
        # Gets end user number
        loader.add_value('unidade_consumidora',self.AUTH_FIELDS.get('sqUnidadeConsumidora'))

        # Checks a valid range date
        if "avisoErro" in response.body:
        
            self.logger.error("Sem registros para esta data na unidade consumidora: %s", self.AUTH_FIELDS['sqUnidadeConsumidora'])

        else:    


            # Get reactive and inductive energy consumption
            reat_checks = response.xpath('//*[@class="fundoLinha2Tabela"]/td[2]/text()').extract()

            # Checks reactive and inductive energy consumption
            if "Ener.Reat.Indutiva" in reat_checks:
                reat = True
 

            if self.new_user:

                # If reactive and inductive energy consumption, gets dates and energy 
                # consumption from different html table cell
                if reat: 

                    # Gets range of energy consumption dates
                    dates = response.xpath('//*[@class="fundoLinha2Tabela"]/td[1]/text()').re('(\A\d{2}/\d{4})$') 

                    # Gets range of interval energy consumption kwh
                    consumption = response.xpath('//*[@class="fundoLinha1Tabela"]/td[3]/text()').re('\d{1,3}')
              
                    # Add list of data into to item loader
                    loader.add_value('consumo_kwh',consumption)
                    loader.add_value('data_referencia',dates)
                    
                    self.logger.info("Unidade consumidora:%s",self.AUTH_FIELDS['sqUnidadeConsumidora'])

                 
                else:
                    
                    # Gets range of energy consumption dates
                    even_dates = response.xpath('//*[@class="fundoLinha2Tabela"]/td[1]/text()').re('(\A\d{2}/\d{4})$')   
                    odd_dates =  response.xpath('//*[@class="fundoLinha1Tabela"]/td[1]/text()').re('(\A\d{2}/\d{4})$')  


                    # Merge even and odd dates
                    marge_dates = [val for pair in zip(even_dates, odd_dates) for val in pair]

                    # Gets range of interval energy consumption kwh
                    consumption_int1 = response.xpath('//*[@class="fundoLinha2Tabela"]/td[3]/text()').re('\d{1,3}')
                    consumption_int2 = response.xpath('//*[@class="fundoLinha1Tabela"]/td[3]/text()').re('\d{1,3}')


                    # Merge interval of energy consumption kwh
                    marge_consumption = [val for pair in zip(consumption_int1, consumption_int2) for val in pair]     
                    

                    # Add list of data into to item loader
                    loader.add_value('consumo_kwh',marge_consumption)
                    loader.add_value('data_referencia',marge_dates)


                    self.logger.info("Unidade consumidora:%s",self.AUTH_FIELDS['sqUnidadeConsumidora'])
            else:

                # If reactive and inductive energy consumption, gets dates and energy 
                # consumption from different html table cell
                if reat: 

                    # Gets range of energy consumption dates
                    dates = response.xpath('//*[@class="fundoLinha2Tabela"]/td[1]/text()').re('(\A\d{2}/\d{4})$') 

                    # Gets range of interval energy consumption kwh
                    consumption = response.xpath('//*[@class="fundoLinha1Tabela"]/td[3]/text()').re('\d{1,3}')
              
                    # Add list of data into to item loader
                    loader.add_value('consumo_kwh',consumption)
                    loader.add_value('data_referencia',dates)

                    self.logger.info("Unidade consumidora:%s",self.AUTH_FIELDS['sqUnidadeConsumidora'])

                else:
                    
                    # Get date of energy and stores on item                
                    dates = response.xpath('//*[@class="fundoLinha2Tabela"]/td[1]/text()').re('(\A\d{2}/\d{4})$')


                    # Get energy consumption and stores on item
                    consumption = response.xpath('//*[@class="fundoLinha2Tabela"]/td[3]/text()').re('\d{1,3}')

                    # Add list of data into items loader
                    loader.add_value('consumo_kwh',consumption)
                    loader.add_value('data_referencia',dates)
            
                    self.logger.info("Unidade consumidora:%s",self.AUTH_FIELDS['sqUnidadeConsumidora'])

        

        return loader.load_item()


@defer.inlineCallbacks
def crawl():

    #Get date today
    today = datetime.date.today()

    #define csv file path
    csv_file_path = "/Volumes/MARCELO/dev_python/pvBot/celesc_kwh_" + today.strftime("%d_%m_%y") + ".csv"

    # Item fields to export in csv file 
    FEED_EXPORT_FIELDS = {"consumo_kwh","data_referencia","user_id"}
    

    runner = CrawlerRunner({
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
    'BOT_NAME': 'celesc',
    # 'LOG_ENABLED': 'True',
    'LOG_FILE':'/Volumes/MARCELO/dev_python/pvBot/crawler_log.txt',
    'FEED_URI': csv_file_path,
    'FEED_FORMAT': 'CSV',
    'FEED_EXPORT_FIELDS': FEED_EXPORT_FIELDS  
    })

    users = [
            {
            'sqUnidadeConsumidora': '44650223',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '04973106941',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '3'
            }, 
            {
            'sqUnidadeConsumidora': '49369557',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '78532272991',
            'senha': 'nico050195',
            'new_user': 'no',
            'id': '6'            
            },
            {
            'sqUnidadeConsumidora': '49331320',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '41723490997',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '7'             
            },
            {
            'sqUnidadeConsumidora': '7817312',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '05503211952',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '8'
            },
            {
            'sqUnidadeConsumidora': '45275086',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '17977525968',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '9'
            },   
            {
            'sqUnidadeConsumidora': '44725495',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '03410425950',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '10'
            },
            {
            'sqUnidadeConsumidora': '46788664',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '06211116942',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '11'
            },
            {
            'sqUnidadeConsumidora': '47711266',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '04698163927',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '12'
            },
            {
            'sqUnidadeConsumidora': '28411502',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '41696956900',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '14'
            },
            {
            'sqUnidadeConsumidora': '3146146',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '02939331979',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '15'
            },
            {
            'sqUnidadeConsumidora': '21312380',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '01822124913',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '20'
            },
            {
            'sqUnidadeConsumidora': '3207757',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '87054248791',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '21'
            },
            {
            'sqUnidadeConsumidora': '22556533',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '04758485941',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '22'
            },
            {
            'sqUnidadeConsumidora': '45418359',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '05165905969',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '24'
            },
            {
            'sqUnidadeConsumidora': '46310705',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '06498676965',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '26'
            },
            {
            'sqUnidadeConsumidora': '45346897',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '04431905936',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '27'
            },
            {
            'sqUnidadeConsumidora': '10775604',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '06480704924',
            'senha': 'gx2dez',
            'new_user': 'no',
            'id': '29'
            },
            {
            'sqUnidadeConsumidora': '46821599',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '02642058980',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '31'
            },            
            {
            'sqUnidadeConsumidora': '845728',
            'tpDocumento':'CPF',
            'numeroDocumentoCPF': '85436968953',
            'senha': 'pagueverde',
            'new_user': 'no',
            'id': '32'
            }                                                                                                                                                                                                                         

    ]    

    for user in users:
        #process.crawl(Celesc, **user)
        yield runner.crawl(Celesc, **user)

    reactor.stop()


def main():

    # csv_file_path = "/Volumes/MARCELO/dev_python/pvBot/celesc_kwh.csv"

    # # Item fields to export in csv file 
    # FEED_EXPORT_FIELDS = {"consumo_kwh","data_referencia","unidade_consumidora"}
    

    # runner = CrawlerRunner({
    # 'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
    # 'BOT_NAME': 'celesc',
    # 'FEED_URI': csv_file_path,
    # 'FEED_FORMAT': 'CSV',
    # 'FEED_EXPORT_FIELDS': FEED_EXPORT_FIELDS  
    # })

    # Set Crawler configuration
    # process = CrawlerProcess({
    # 'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
    # 'BOT_NAME': 'celesc',
    # 'FEED_URI': csv_file_path,
    # 'FEED_FORMAT': 'CSV',
    # 'FEED_EXPORT_FIELDS': FEED_EXPORT_FIELDS  
    # })
  

    # users = [
    #         {
    #         'sqUnidadeConsumidora': '49369557',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '78532272991',
    #         'senha': 'nico050195',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '45275086',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '17977525968',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '45346897',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '04431905936',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '47711266',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '04698163927',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '28411502',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '41696956900',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '3146146',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '02939331979',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '44650223',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '04973106941',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         }           

    # ]

    #user = {
    #        'sqUnidadeConsumidora': '44650223',
    #        'tpDocumento':'CPF',
    #        'numeroDocumentoCPF': '04973106941',
    #        'senha': 'pagueverde',
    #        'new_user': 'yes'

    #}

    #process.crawl(Celesc, **user)
    



    # Create crawling a process
    # for user in users:
    #     process.crawl(Celesc, **user)

    # Start process
    #process.start()

    configure_logging()
    
    crawl()
    
    reactor.run() 



if  __name__ =='__main__':
     main()