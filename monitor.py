# -*- coding: UTF-8 -*-


import getopt, sys, os
import csv
import datetime
import copy

from twisted.internet import reactor,defer
import datetime
from celesc import Celesc
from copel import Copel
from scrapy.utils.log import configure_logging
from scrapy.crawler import CrawlerRunner


@defer.inlineCallbacks
def crawl(runner, users, spider):
    for user in users:
        yield runner.crawl(spider, **user)
    reactor.stop()


def run_bot(reg_list, operation, dist):
    
    #Get date today
    today = datetime.date.today()

    # Output csv path
    csv_file_path = "" 

    # Feed fields configs
    FEED_EXPORT_FIELDS = set()

    # Checks operation to define file name of exported csv and fields
    if operation is 'v': 

        csv_file_path = str(os.getcwd())+ "/validate_" + today.strftime("%d_%m_%y") + ".csv"
        FEED_EXPORT_FIELDS = {"status","user_id","unidade_consumidora","cpf","email", "senha"}

    elif operation is 'c':

        csv_file_path = str(os.getcwd())+"/"+dist+"_kwh_" + today.strftime("%d_%m_%y") + ".csv"
        FEED_EXPORT_FIELDS = {"consumo_kwh","data_referencia","user_id"}


    elif operation is 'h':
        csv_file_path = str(os.getcwd())+"/historico_de_consumo_"+dist+"_"+ today.strftime("%d_%m_%y") + ".csv"
        FEED_EXPORT_FIELDS = {"data_referencia","unidade_consumidora","consumo_kwh"}

    EXTENSIONS = {
        'scrapy.telnet.TelnetConsole': None,
        'scrapy.extensions.corestats.CoreStats': 0,
        # 'scrapy.extensions.telnet.TelnetConsole': 0,
        'scrapy.extensions.memusage.MemoryUsage': 0,
        'scrapy.extensions.memdebug.MemoryDebugger': 0,
        'scrapy.extensions.closespider.CloseSpider': 0,
        'scrapy.extensions.feedexport.FeedExporter': 0,
        'scrapy.extensions.logstats.LogStats': 0,
        'scrapy.extensions.spiderstate.SpiderState': 0,
        'scrapy.extensions.throttle.AutoThrottle': 0,
    }
    
    

    # SELENIUM_DRIVER_NAME='chrome'
    # SELENIUM_DRIVER_EXECUTABLE_PATH=which('chromedriver')
    # SELENIUM_DRIVER_ARGUMENTS=['-headless']

    # Configure Crawler
    runner = CrawlerRunner({
        'USER_AGENT': 'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 5.1)',
        'BOT_NAME': 'celesc',
        'FEED_URI': csv_file_path,
        'FEED_FORMAT': 'csv',
        'FEED_EXPORT_FIELDS': FEED_EXPORT_FIELDS,
        'EXTENSIONS_BASE': EXTENSIONS,
        'DOWNLOADER_MIDDLEWARES': {'middleware.SeleniumMiddleware': 800},
        'SELENIUM_DRIVER_NAME': 'firefox',
        'SELENIUM_DRIVER_EXECUTABLE_PATH': 'geckodriver',
        'SELENIUM_DRIVER_ARGUMENTS': ['--headless'],

    })

    # Checks 
    if "celesc" in dist:
        # Configure log file from energy utilies
        configure_logging({'LOG_FILE': 'celesc.log'})

        # Launch crawler
        crawl(runner, reg_list, Celesc)
    elif "copel"  in dist:
        # Configure log file from energy utilies
        configure_logging({'LOG_FILE': 'copel.log'})  
        # Launch crawler
        crawl(runner, reg_list, Copel)
    
    # Run spiders sequentially
    reactor.run()


def main(argv):

    # Options:
    # --help : Help
    # -d : Energy utilities 
    # -h : Energy consumption history
    # -v : validation 
    # -c : Current energy consumption
    # -l : CSV file with user list 

    # Command line args
    args_list = 'd:hvcf:'

    # Energy utilities
    dist = ''

    # Operations
    operation = ''
    
    # CSV file
    input_file = ''

    # Current directory 
    curr_dir = os.getcwd()
    
    # Get args
    try:
        opts, args = getopt.getopt(argv,args_list,["help="])
    except getopt.GetoptError:
      print ('\nmonitor.py -d <energy_utilities>  -<operation> -f <csv_file>\n')
      print ('   -d <energy_utilities> : Energy Utilities supported: celesc\n')
      print ('   -<operation> : -h to consumption history, -v to validation and -c to current energy consumption\n')
      print ('   -f or --csv_file: CSV file with user list\n')
      print ('   --help: Help command line arguments\n')
      sys.exit(2)

    # Checks arg list options
    for opt, arg in opts:
        if opt == "-help":
            print ('\nmonitor.py -d <energy_utilities>  -<operation> -f <csv_file>\n')
            print ('   -d <energy_utilities> : Energy Utilities supported: celesc\n')
            print ('   -<operation> : -h to consumption history, -v to validation and --c to current energy consumption\n')
            print ('   -f or --csv_file: CSV file with user list\n')
            print ('   --help: Help command line arguments\n')
            sys.exit()
        elif opt in "-d":
            dist = arg
        elif opt in "-h":
            operation = "h"
            print(arg)
        elif opt in  "-v":
            operation = "v"
            print(arg)
        elif opt in  "-c":
            operation = "c"                     
        elif opt in ("-f","--csv_file"):
            input_file = arg


    # List of csv registers
    csv_registers = list()


    # Checks operation to read rightly csv file registers
    if operation is "v":
        
        #Read csv file with new users data to validade
        with open(input_file) as csvfile:
             reader = csv.DictReader(csvfile, fieldnames=['Id', 'NumeroInstalacao',
                'CpfTitular','DataNacimentoTitular','TelefoneTitular','EmailTitular'])
             for row in reader:
                
                info_scratch = {
                'id': row['Id'],
                'sqUnidadeConsumidora': row['NumeroInstalacao'],
                'tpDocumento':'CPF',
                'numeroDocumentoCPF': row['CpfTitular'],
                'descEmail': row['EmailTitular'],
                'dtaNascimento':row['DataNacimentoTitular'],
                'celular':row['TelefoneTitular']
                }                
                
                csv_registers.append(info_scratch)
        # Discart field names of csv file
        csv_registers.pop(0)        

    elif operation is "h":


        #Read csv file with new users data to validade
        with open(input_file, 'rb') as csvfile:
             
            reader = csv.DictReader(csvfile)

            for row in reader:
                
                if len(row['CPF do titular da conta']) == 10 :
                    
                    cpf = "0" + row['CPF do titular da conta']
                
                elif len(row['CPF do titular da conta']) == 9 :

                    cpf = "00" + row['CPF do titular da conta']
                
                else :    
                    cpf = row['CPF do titular da conta']

                info_scratch = {
                'sqUnidadeConsumidora': row['Nº de instalação'],
                'tpDocumento':'CPF',
                'numeroDocumentoCPF': cpf,
                'senha': row['senha'],
                'nome': row['Primeiro nome'],
                'sobrenome': row['Segundo nome'],
                'descEmail': row['E-mail que você usa com maior frequencia'],
                'celular': row['Nº do Celular'],
                'new_user': "yes",
                'id': row['id']
                }
                
                csv_registers.append(info_scratch)

    elif operation is "c":

        #Read csv file with new users data to validade
        with open(input_file, 'rb') as csvfile:
             
            reader = csv.DictReader(csvfile)

            for row in reader:
                
                if len(row['CPF do titular da conta']) == 10 :
                    
                    cpf = "0" + row['CPF do titular da conta']
                
                elif len(row['CPF do titular da conta']) == 9 :

                    cpf = "00" + row['CPF do titular da conta']
                
                else :    
                    cpf = row['CPF do titular da conta']

                info_scratch = {
                'sqUnidadeConsumidora': row['Nº de instalação'],
                'tpDocumento':'CPF',
                'numeroDocumentoCPF': cpf,
                'senha': row['senha'],
                'nome': row['Primeiro nome'],
                'sobrenome': row['Segundo nome'],
                'id': row['id']
                }
                
                csv_registers.append(info_scratch)                

    # Run crawler
    run_bot(csv_registers, operation, dist)
                                     


if  __name__ =='__main__':
     main(sys.argv[1:])