import scrapy
import csv
import datetime
import urllib

from twisted.internet import reactor,defer

from scrapy.crawler import CrawlerProcess
from scrapy.http import FormRequest
from scrapy.http import Request
from scrapy import Item, Field
from scrapy.loader import ItemLoader

from selenium import webdriver 

# Itens of spider class
class Consumption(scrapy.Item):

    user_id = scrapy.Field()
    unidade_consumidora = scrapy.Field()
    data_referencia = scrapy.Field()
    consumo_kwh  = scrapy.Field()


# Celesc Spider crawler class
class Eletropaulo(scrapy.Spider):
    name = "eletropaulo"
    

    
    allowed_domains = [
                        "https://www.aeseletropaulo.com.br/para-sua-casa/conteudo/para-sua-casa",
                        "https://www.aeseletropaulo.com.br/para-sua-casa/agencia-virtual/servicos/instalacao"
    ]
    
    start_urls = (
        'https://www.aeseletropaulo.com.br/para-sua-casa/conteudo/para-sua-casa',
    )


    # Redefine to receive dictionary of user data
    def __init__(self, *args, **kwargs):
        super(Eletropaulo, self).__init__(*args, **kwargs)
 


        # Selenium webdriver
        self.driver = webdriver.Firefox()

         # Dictionary of user authentication fileds
        self.AUTH_FIELDS = dict()
        
        # Dictionary of user password field
        # self.PASSWD_FIELD = dict()

        # # Field search of energy consumption
        # self.HIST_RANGE = dict()

        # # User Id
        # self.id = kwargs.get('id')

        # Indicates that a new user, so we have to get energy consumption range during a period
        self.new_user = False       

        # Unpacking user data from login
        self.AUTH_FIELDS['ctl00$SPWebPartManager1$g_c0994853_c7b6_40ff_a746_49085e06a119$ctl00$EditModePanel1$txtCPF'] = kwargs.get('ctl00$SPWebPartManager1$g_c0994853_c7b6_40ff_a746_49085e06a119$ctl00$EditModePanel1$txtCPF')
        self.AUTH_FIELDS['ctl00$SPWebPartManager1$g_c0994853_c7b6_40ff_a746_49085e06a119$ctl00$EditModePanel1$txtNumeroInstalacao'] = kwargs.get('ctl00$SPWebPartManager1$g_c0994853_c7b6_40ff_a746_49085e06a119$ctl00$EditModePanel1$txtNumeroInstalacao')
        self.AUTH_FIELDS['type'] = 'submit'
        #self.AUTH_FIELDS['id'] = kwargs.get('id')
        #self.id= kwargs.get('id')


        
        # Check historic 
        # if 'yes' in kwargs.get('new_user'):
                
        #     # update state of new user
        #     self.new_user = True

        # else:  

        #     # update state of new user
        #     self.new_user = False
        # pass



    def parse(self, response):
        #logging.log(logging.INFO,"Efetuando login unidade consumidora e CPF...")
        self.logger.info("Efetuando login unidade consumidora e CPF...")

        # Get response url
        self.driver.get(response.url)
        
        img = self.driver.find_element_by_xpath('//div[@id="recaptcha_image"]/img')
        
        src = img.get_attribute('src')
        # download the image
        urllib.urlretrieve(src, "eletropaulo_captcha.png")
        
        self.driver.quit()
        
        # Pausar o crawler
        #self.crawler.engine.pause()
        
        # Resolver o captcha aqui


        # Preencher os campos CPF, Numero de Instalacao e Captcha
        # cpf_field = self.driver.find_element_by_id('ctl00$SPWebPartManager1$g_c0994853_c7b6_40ff_a746_49085e06a119$ctl00$EditModePanel1$txtCPF')
        # cpf_field.click()
        # cpf_field.clear()
        # cpf_field.send_keys('foo') # Colocar CPF aqui

        # n_client = self.driver.find_element_by_id('ctl00$SPWebPartManager1$g_c0994853_c7b6_40ff_a746_49085e06a119$ctl00$EditModePanel1$txtNumeroInstalacao')
        # n_client.click()
        # n_client.clear()
        # n_client.send_keys('foo') # Colocar o numero de instalacao aqui aqui

        # Pegar a resposta do Selenium e passa pro scrapy para seguir crawleando
        #source = self.browser.page_source 
        # Ver o link: http://stackoverflow.com/questions/27585068/scrapy-with-selenium-crawling-but-not-scraping

        # Despausar o crawler
        #self.crawler.engine.unpause()


        return response



def main():

    # csv_file_path = "/Volumes/MARCELO/dev_python/pvBot/celesc_kwh.csv"

    # # Item fields to export in csv file 
    # FEED_EXPORT_FIELDS = {"consumo_kwh","data_referencia","unidade_consumidora"}
    

    # runner = CrawlerRunner({
    # 'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
    # 'BOT_NAME': 'celesc',
    # 'FEED_URI': csv_file_path,
    # 'FEED_FORMAT': 'CSV',
    # 'FEED_EXPORT_FIELDS': FEED_EXPORT_FIELDS  
    # })

    # Set Crawler configuration
    process = CrawlerProcess({
    'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)',
    'BOT_NAME': 'celesc',
    #'FEED_URI': csv_file_path,
    #'FEED_FORMAT': 'CSV',
    #'FEED_EXPORT_FIELDS': FEED_EXPORT_FIELDS  
    })
  

    # users = [
    #         {
    #         'sqUnidadeConsumidora': '49369557',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '78532272991',
    #         'senha': 'nico050195',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '45275086',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '17977525968',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '45346897',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '04431905936',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '47711266',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '04698163927',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '28411502',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '41696956900',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '3146146',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '02939331979',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         },
    #         {
    #         'sqUnidadeConsumidora': '44650223',
    #         'tpDocumento':'CPF',
    #         'numeroDocumentoCPF': '04973106941',
    #         'senha': 'pagueverde',
    #         'new_user': 'no'
    #         }           

    # ]

    user = {
           'ctl00$SPWebPartManager1$g_c0994853_c7b6_40ff_a746_49085e06a119$ctl00$EditModePanel1$txtCPF"': '26138091833',
           'ctl00$SPWebPartManager1$g_c0994853_c7b6_40ff_a746_49085e06a119$ctl00$EditModePanel1$txtNumeroInstalacao':'119939371',
    }

    process.crawl(Eletropaulo, **user)
    



    # Create crawling a process
    # for user in users:
    #     process.crawl(Celesc, **user)

    # Start process
    process.start()

    #configure_logging()
    
    #crawl()
    
    #reactor.run() 



if  __name__ =='__main__':
     main()