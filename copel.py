# -*- coding: UTF-8 -*-

import re
import scrapy
import string
from scrapy.http import FormRequest
from scrapy.http import Request
from scrapy import Item, Field
from scrapy.loader import ItemLoader
import csv



# Itens of spider class
class Validate(scrapy.Item):

    unidade_consumidora = scrapy.Field()
    cpf = scrapy.Field()
    email = scrapy.Field()
    status = scrapy.Field()
    senha = scrapy.Field()
    user_id = scrapy.Field()


# Itens of spider class
class Consumption(scrapy.Item):

    user_id = scrapy.Field()
    unidade_consumidora = scrapy.Field()
    data_referencia = scrapy.Field()
    consumo_kwh  = scrapy.Field()
    nome = scrapy.Field()
    sobrenome = scrapy.Field()
    cpf = scrapy.Field()
    senha = scrapy.Field()
    email = scrapy.Field()
    celular = scrapy.Field()


# Celesc Spider crawler class
class Copel(scrapy.Spider):
    name = "celesc"
    
    password_url = "http://www.copel.com/AgenciaWeb/autenticar/autenticar.do"
    

    hist_consumption = "http://www.copel.com/AgenciaWeb/consultarHistConsumoGrupoB/consultarHistConsumoGrupoB.do"
    
    allowed_domains = [
                        "http://www.copel.com/AgenciaWeb/autenticar/loginCliente.do",
                        "http://www.copel.com/AgenciaWeb/autenticar/autenticar.do",
                        "http://www.copel.com/AgenciaWeb/consultarHistoricoConsumo/histCons.do"
    ]
    
    start_urls = (
        'http://www.copel.com/AgenciaWeb/autenticar/loginCliente.do',
    )


    # Redefine to receive dictionary of user data
    def __init__(self, *args, **kwargs):
        super(Copel, self).__init__(*args, **kwargs)
 

        # Dictionary of user authentication fileds
        self.AUTH_FIELDS = dict()
        
        # Dictionary of user first acess
        self.ACESS_FILED = dict()

        # Dictionary of user password field
        self.PASSWD_FIELD = dict()

        # Field search of energy consumption
        self.HIST_RANGE = dict()

        # Unpacking User Id
        if kwargs.get('id') is not None:
            self.id = kwargs.get('id')

        # Unpacking User e-mail
        if kwargs.get('descEmail') is not None:
            self.email = kwargs.get('descEmail')
        
        # Indicates that a new user, so we have to get energy consumption range during a period
        self.new_user = False       


        # Unpacking user data from login
        self.AUTH_FIELDS['sqUnidadeConsumidora'] = kwargs.get('sqUnidadeConsumidora')
        self.AUTH_FIELDS['tpDocumento'] = kwargs.get('tpDocumento')
        cpf = kwargs.get('numeroDocumentoCPF')
        #Check CPF pattern 000.000.000-00
        if re.match(r'^^[0-9]{3}\.[0-9]{3}\.[0-9]{3}\-[0-9]{2}$',cpf) is not None:
            self.AUTH_FIELDS['numeroDocumentoCPF'] = cpf.translate(None, ''.join(['.', '-']))
        else:
            self.AUTH_FIELDS['numeroDocumentoCPF'] = cpf
        
        # Unpacking user data from first acess
        if kwargs.get('dtaNascimento') is not None:
            
            # Put birth date on appropriate format
            birth_date = kwargs.get('dtaNascimento')[8:10] + '/' \
            + kwargs.get('dtaNascimento')[5:7] + '/' + kwargs.get('dtaNascimento')[:4]
            
            # Fill first acess form
            self.ACESS_FILED['dtaNascimento'] =  birth_date
            self.ACESS_FILED['senhaNova'] = 'pagueverde'
            self.ACESS_FILED['senhaConfirmacao'] = 'pagueverde'
            self.ACESS_FILED['lembrete'] = 'sua senha e pagueverde'
            

        # Unpacking password user data 
        if kwargs.get('senha') is not None:
            self.PASSWD_FIELD['senha'] = kwargs.get('senha')

        # unpacking cell phone    
        if kwargs.get('celular') is not None:
            self.cellphone = kwargs.get('celular')
        
        # Unpacking first and second name
        if kwargs.get('nome') is not None:
            self.fisrt_name = kwargs.get('nome')
        
        if kwargs.get('sobrenome') is not None:
            self.second_name = kwargs.get('sobrenome')


        # Check historic 
        if kwargs.get('new_user') is not None:

            if 'yes' in kwargs.get('new_user'):
                    
                # update state of new user
                self.new_user = True

            else:  

                # update state of new user
                self.new_user = False
            pass


    # Acess first level to login with CPF and energy utilities user number
    def parse(self, response):
        self.logger.info("Efetuando login unidade consumidora e CPF...")

        return [FormRequest.from_response(
                response,
                formdata=self.AUTH_FIELDS,
                callback=self.password_login,
                dont_filter=True
        )]
   
    # Check first level to loging and validate new energy utilities users 
    def password_login(self, response):

        # Checks data succeed before going on
        self.logger.info("Verificando dados de acesso....")


        # Checks register udate for energy utilities users 
        if "Alterar Dados Cadastrais" in response.body:
            
            self.logger.info("Alterar dados cadastrais....")

            # Get fields
            ddd_telefone = response.xpath('//*[@id="dddTelefone"]/@value').extract()
            prefixo_telefone = response.xpath('//*[@id="prefixoTelefone"]/@value').extract()
            sufixo_telefone = response.xpath('//*[@id="sufixoTelefone"]/@value').extract()
            ddd_celular = response.xpath('//*[@id="ddd_celular"]/@value').extract()
            prefixo_celular = response.xpath('//*[@id="prefixo_celular "]/@value').extract()
            sufixo_celular = response.xpath('//*[@id="sufixo_celular"]/@value').extract()
            desc_email = response.xpath('//*[@id="descEmail"]/@value').extract()
            desc_email_alter = response.xpath('//*[@id="descEmailAlter"]/@value').extract()

            self.logger.info(desc_email)

            # Checks empty email field
            if desc_email is not None:
                desc_email = self.email
                desc_email_alter = self.email


            # Checks empty Cell phone field
            if ddd_celular is not None:
                
                # Here is a ugly way to check cell phone number pattern
                # Where DD is code area, PPPP is for prefix number and SSSS is for sufix number 

                # Checks cell phone pattern (DD)PPPP-SSSS
                if re.match(r'^\([1-9]{2}\)[2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:
                    
                    ddd_celular =  self.cellphone[1:3]
                    prefixo_celular = self.cellphone[5:9]
                    sufixo_celular = self.cellphone[10:15]

                # Checks numeric pattern of cell phone number (DD) PPPP-SSSS
                elif re.match(r'^\([1-9]{2}\) [2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:
                                         
                    ddd_celular = self.cellphone[1:3]
                    prefixo_celular = self.cellphone[5:9]
                    sufixo_celular = self.cellphone[10:14]
                
                # Checks numeric pattern of cell phone number DDPPPPSSSS
                elif re.match(r'^[1-9]{2}[2-9][0-9]{3,4}[0-9]{4}$', self.cellphone) is not None:
                    
                    ddd_celular = self.cellphone[:2]
                    prefixo_celular = self.cellphone[2:6]
                    sufixo_celular = self.cellphone[6:11]                  

                # Checks numeric pattern of cell phone number (DD)PPPPSSSS
                elif re.match(r'^\([1-9]{2}\)[2-9][0-9]{3,4}[0-9]{4}$', self.cellphone) is not None:
                
                    ddd_celular = self.cellphone[1:3]
                    prefixo_celular = self.cellphone[4:8]
                    sufixo_celular = self.cellphone[8:13]
                                           
                # Checks numeric pattern of cell phone number DD-PPPPSSSS
                elif re.match(r'^[1-9]{2}\-[2-9][0-9]{3,4}[0-9]{4}$', self.cellphone) is not None:
                    
                    self.logger.info(self.cellphone)

                    ddd_celular =  self.cellphone[:2]
                    prefixo_celular = self.cellphone[3:7]
                    sufixo_celular = self.cellphone[7:11]

                # Checks numeric pattern of cell phone number DD-PPPP-SSSS
                elif re.match(r'^[1-9]{2}\-[2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = self.cellphone[:2]
                    prefixo_celular = self.cellphone[3:7]
                    sufixo_celular = self.cellphone[8:12]

                # Checks numeric pattern of cell phone number DD PPPP-SSSS
                elif re.match(r'^[1-9]{2} [2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = self.cellphone[:2]
                    prefixo_celular = self.cellphone[3:7]
                    sufixo_celular = self.cellphone[8:12]
                    
                # Checks numeric pattern of cell phone number 0DDPPPPSSSS
                elif re.match(r'^0[1-9]{2}[2-9][0-9]{3,4}[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = self.cellphone[1:3]
                    prefixo_celular = self.cellphone[3:7]
                    sufixo_celular = self.cellphone[7:12]


                # Checks numeric pattern of cell phone number DD PPPPSSSS
                elif re.match(r'^[1-9]{2} [2-9][0-9]{3,4}[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = self.cellphone[:2]
                    prefixo_celular = self.cellphone[3:7]
                    sufixo_celular = self.cellphone[7:12]

                # Checks numeric pattern of cell phone number DD PPPP SSSS
                elif re.match(r'^[1-9]{2} [2-9][0-9]{3,4} [0-9]{4}$', self.cellphone) is not None:
                    
                    ddd_celular = self.cellphone[:2]
                    prefixo_celular = self.cellphone[3:7]
                    sufixo_celular = self.cellphone[8:12]

                # Checks numeric pattern of cell phone number DD- PPPP-NNNN
                elif re.match(r'^[1-9]{2}\- [2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = self.cellphone[:2]
                    prefixo_celular = self.cellphone[4:8]
                    sufixo_celular = self.cellphone[9:13]

                # Checks numeric pattern of cell phone number 0DD-PPPP-SSSS
                elif re.match(r'^0[1-9]{2}\-[2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = self.cellphone[1:3]
                    prefixo_celular = self.cellphone[4:8]
                    sufixo_celular = self.cellphone[9:13]

                # Checks numeric pattern of cell phone number 0DD PPPPSSSS
                elif re.match(r'^0[1-9]{2} [2-9][0-9]{3,4}[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = self.cellphone[1:3]
                    prefixo_celular = self.cellphone[4:8]
                    sufixo_celular = self.cellphone[8:13]


                # Checks numeric pattern of cell phone number 0DD PPPP-SSSS
                elif re.match(r'^0[1-9]{2} [2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = self.cellphone[1:3]
                    prefixo_celular = self.cellphone[4:8]
                    sufixo_celular = self.cellphone[9:13]

                 # Checks numeric pattern of cell phone number (0DD)9PPPP-SSSS
                elif re.match(r'^\(0[1-9]{2}\)[2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:
                    
                    ddd_celular = self.cellphone[2:4]
                    prefixo_celular = self.cellphone[5:9]
                    sufixo_celular = self.cellphone[11:15]

                 # Checks numeric pattern of cell phone number PPPP-SSSS
                elif re.match(r'^[2-9][0-9]{3,4}\-[0-9]{4}$', self.cellphone) is not None:

                    ddd_celular = '48'
                    prefixo_celular = self.cellphone[:4]
                    sufixo_celular = self.cellphone[5:]


                 # Checks numeric pattern of cell phone number PPPPSSSS
                elif re.match(r'^[2-9][0-9]{3,4}[0-9]{4}$', self.cellphone) is not None:
                    self.logger.info(self.cellphone)

                    ddd_celular = '48'
                    prefixo_celular = self.cellphone[:4]
                    sufixo_celular = self.cellphone[4:]

                # Checks numeric pattern of cell phone number (DD) PPPP-SSSS_
                elif re.match(r'^\([1-9]{2}\) [2-9][0-9]{3,4}\-[0-9]{4}\_$', self.cellphone) is not None:
                                         
                    ddd_celular = self.cellphone[1:3]
                    prefixo_celular = self.cellphone[5:9]
                    sufixo_celular = self.cellphone[10:14]
                

            ALT_REGISTER = dict()
            
            ALT_REGISTER['dddTelefone'] = ddd_telefone
            ALT_REGISTER['prefixoTelefone'] = prefixo_telefone
            ALT_REGISTER['sufixoTelefone'] = sufixo_telefone
            ALT_REGISTER['dddCelular'] = ddd_celular
            ALT_REGISTER['prefixoCelular '] = prefixo_celular 
            ALT_REGISTER['sufixoCelular'] = sufixo_celular
            ALT_REGISTER['descEmail'] = desc_email
            ALT_REGISTER['descEmailAlter'] = desc_email_alter
            

            self.logger.info(ALT_REGISTER)
            
            return [FormRequest.from_response(
                    response,
                    formdata = ALT_REGISTER,
                    callback=self.check_register_update, dont_filter=True
            )]            


        # Checks new users utilities that who aready have password
        if not self.PASSWD_FIELD:

            if "senha de acesso" in response.body:
                self.logger.info("Ja tem senha criada....")

                # Loader data into Consumption Items
                loader = ItemLoader(item=Validate(), response=response)    

                # Gets error status
                loader.add_value('status',"Ja possui senha cadastrada")

                #Get user email
                loader.add_value('email',self.email) 

                # Gets user utilities 
                loader.add_value('cpf',self.AUTH_FIELDS.get('numeroDocumentoCPF'))

                # Gets user utilities 
                loader.add_value('unidade_consumidora',self.AUTH_FIELDS.get('sqUnidadeConsumidora'))   
                
                            # Gets user id
                if hasattr(self, 'id'):
                    loader.add_value('user_id',self.id)
                    
                return loader.load_item() 
            

        # Checks first login
        if "primeiro login" in response.body:
            
            self.logger.info("Criando a senha do primeiro acesso... ")

            # Go to vailidate first login
            return [FormRequest.from_response(
                    response,
                    formdata = self.ACESS_FILED,
                    callback=self.first_login, dont_filter=True
            )]
  

        # Checks CPF and user utilities number errors
        if "avisoErro" in response.body:

            # Loader data into Consumption Items
            loader = ItemLoader(item=Validate(), response=response)
            
            # Checks user utilities CPF error    
            if "documentos do titular" in response.body:
                
                self.logger.warning("Existe divergencia nos documentos do titular - CPF: %s UC: %s", self.AUTH_FIELDS['numeroDocumentoCPF'], self.AUTH_FIELDS['sqUnidadeConsumidora'])
                
                # Gets error status
                loader.add_value('status',"CPF divergente do titular ou incorreto")
            

            # Checks user utilities number error
            if "medidor" in response.body:
                
                self.logger.warning("Numero de unidade consumidora incorreto ou invalido")
                
                # Gets error status
                loader.add_value('status',"Numero de unidade consumidora incorreto ou invalido")


            #Get user email
            if hasattr(self, 'email'):
                loader.add_value('email',self.email) 

            # Gets user utilities 
            loader.add_value('cpf',self.AUTH_FIELDS.get('numeroDocumentoCPF'))

            # Gets user utilities 
            loader.add_value('unidade_consumidora',self.AUTH_FIELDS.get('sqUnidadeConsumidora'))               

            # Gets user id
            if hasattr(self, 'id'):
                loader.add_value('user_id',self.id)
            
            return loader.load_item()    
 
        return [FormRequest.from_response(
                response,
                formdata = self.PASSWD_FIELD,
                callback=self.after_login, dont_filter=True
        )]

    # Checks update registry from new energy utilities users
    def check_register_update(self,response):
        
        self.logger.info("Verificando atualizacao de cadastro")

        if "avisoErro" in response.body:

            # Loader data into Consumption Items
            loader = ItemLoader(item=Validate(), response=response)

            type_error = response.xpath('//span[@class="textoErroMensagem"]/text()').extract()
            self.logger.warning(type_error)

            for error in type_error:

                if "Erro ao validar o DDD do Celular" in error:

                    self.logger.warning("Erro ao validar o DDD do Celular")

                    # Gets error status
                    loader.add_value('status',"Erro ao validar o DDD do Celular")

                elif "campo E-mail" in error:

                    self.logger.warning("E-mail do titular ausente")
                    
                    # Gets error status
                    loader.add_value('status',"E-mail do titular ausente")                    

                elif "campo DDD do telefone" in error:
                    
                    self.logger.warning("DDD do telefone ausente")

                    # Gets error status
                    loader.add_value('status',"DDD do telefone ausente")

                elif "campo Prefixo do telefone" in error:

                    self.logger.warning("Prefixo do telefone ausente")

                    # Gets error status
                    loader.add_value('status',"Prefixo do telefone ausente")

                elif "Favor preencher corretamente o numero do telefone celular" in error:

                    self.logger.warning("Numero do telefone celular preenchido incorretamente")

                    # Gets error status
                    loader.add_value('status',"Numero do telefone celular preenchido incorretamente")

            #Get user email
            if hasattr(self, 'email'):
                loader.add_value('email',self.email) 

            # Gets user utilities 
            loader.add_value('cpf',self.AUTH_FIELDS.get('numeroDocumentoCPF'))

            # Gets user utilities 
            loader.add_value('unidade_consumidora',self.AUTH_FIELDS.get('sqUnidadeConsumidora'))   

            # Gets user id
            if hasattr(self, 'id'):
                loader.add_value('user_id',self.id) 

            return loader.load_item() 

        elif 'seu primeiro login' in response.body:

            self.logger.info("Criando a senha do primeiro acesso... ")

            # Go to vailidate first login
            return [FormRequest.from_response(
                    response,
                    formdata = self.ACESS_FILED,
                    callback=self.first_login, dont_filter=True
            )]
       

    # Check first login acess and validate 
    def first_login(self, response):
        
        # Checks first login before going on
        self.logger.info("Verificando senha do primeiro acesso....")


        # Loader data into Validate Items
        loader = ItemLoader(item=Validate(), response=response)

        # Checks birth date error
        if "avisoErro" in response.body:
            
            if "Data de nascimento informada" in response.body:
                
                self.logger.warning("A data de nascimento incorreta da unidade consumidora:%s", self.AUTH_FIELDS['sqUnidadeConsumidora'])
                #self.logger.info(self.ACESS_FILED['dtaNascimento'] )
                
                # Gets error status
                loader.add_value('status',"Data de nascimento incorreta")
        else:

            self.logger.warning("Senha criado com sucesso para unidade consumidora:%s", self.AUTH_FIELDS['sqUnidadeConsumidora'])
            
            # Gets status
            loader.add_value('status',"Senha criada")
            # Gets password
            loader.add_value('senha',"pagueverde")
        
        #Get user email
        if hasattr(self, 'email'):
            loader.add_value('email',self.email) 

        # Gets user utilities 
        loader.add_value('cpf',self.AUTH_FIELDS.get('numeroDocumentoCPF'))

        # Gets user utilities 
        loader.add_value('unidade_consumidora',self.AUTH_FIELDS.get('sqUnidadeConsumidora')) 
        
        # Gets user id
        if hasattr(self, 'id'):
            loader.add_value('user_id',self.id)

        return loader.load_item()    


    # Check login password
    def after_login(self, response):
        self.logger.info("Verificando login....")
        
        # Checks login error
        if "avisoErro" in response.body:
            #logging.log(logging.ERROR, "Login failed")
            self.logger.error("Login falhou na unidade consumidora:%s", self.AUTH_FIELDS['sqUnidadeConsumidora'])

            return
        else:

            self.logger.info("Login efetuado com sucesso...")
            
            if self.new_user :

                return Request(url=self.hist_consumption,
                               callback=self.consumption_history,
                               dont_filter=True)
            else :

                return Request(url=self.hist_consumption,
                               callback=self.last_consumption,
                               dont_filter=True)  

    # Gets month of reference
    def consumption_history(self, response):
        
        #logging.log(logging.INFO,"Pegando o mes de referencia..")
        self.logger.info("Pegando o historico de consumo..")

        # Loader data into Consumption Items
        loader = ItemLoader(item=Consumption(), response=response)

        if hasattr(self, 'id'):
            loader.add_value('unidade_consumidora',str(self.id) )


        # Gets range of energy consumption dates
        even_dates = response.xpath('//*[@class="fundoLinha2Tabela"]/td[1]/text()').extract()
        # Strip \r\n characters
        even_dates = [x.strip() for x in even_dates]
        odd_dates =  response.xpath('//*[@class="fundoLinha1Tabela"]/td[1]/text()').extract() 
        # Strip \r\n characters
        odd_dates = [x.strip() for x in odd_dates]

        # Merge even and odd dates
        dates = [val for pair in zip(even_dates, odd_dates) for val in pair]

        # Gets range of interval energy consumption kwh
        consumption_int1 = response.xpath('//*[@class="fundoLinha2Tabela"]/td[7]/text()').extract()
        consumption_int1 = [x.strip() for x in consumption_int1]
        consumption_int2 = response.xpath('//*[@class="fundoLinha1Tabela"]/td[7]/text()').extract()
        consumption_int2 = [x.strip() for x in consumption_int2]

        # Merge interval of energy consumption kwh
        consumption = [val for pair in zip(consumption_int1, consumption_int2) for val in pair]   

        # Add list of data into to item loader
        loader.add_value('consumo_kwh',consumption)
        loader.add_value('data_referencia',dates)
        
        #self.logger.info(marge_consumption)

        # Load item 
        return loader.load_item()   

    # Gets consumption range
    def last_consumption(self, response):
        
        #logging.log(logging.INFO,"Pesquisando o historico de consumo..")
        self.logger.info("Pegando o consumo do mes..")

        # Loader data into Consumption Items
        loader = ItemLoader(item=Consumption(), response=response)

        if hasattr(self, 'id'):
            loader.add_value('user_id',str(self.id) )

        consumption = response.xpath('//*[@class="fundoLinha2Tabela"]/td[7]/text()').extract_first()
        consumption = consumption.split()

        date = response.xpath('//*[@class="fundoLinha2Tabela"]/td[1]/text()').extract_first()
        date  = date.split()


        # Add list of data into to item loader
        loader.add_value('consumo_kwh',consumption)
        loader.add_value('data_referencia',date)
        
        # Load item 
        return loader.load_item()